import AntdScssThemePlugin from 'antd-scss-theme-plugin';

const webpackConfig = {
  // ...
  plugins: [
    new AntdScssThemePlugin('./theme.scss'),
  ],
};


module.exports = {
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: process.env.NODE_ENV !== 'production',
            },
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              sourceMap: process.env.NODE_ENV !== 'production',
            },
          },
          AntdScssThemePlugin.themify({
            loader: 'sass-loader',
            options: {
              sourceMap: process.env.NODE_ENV !== 'production',
            },
          })
        ],
      },
    ],
  },
};